package com.github.andx2.eastfoods.pojo;

/**
 * Created by savos on 19.11.2016.
 */
public class Food {
    private String title;
    private String urlSuffix;
    private String avatar;
    private String pic;
    private String cost;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getUrl() {
        return urlSuffix;
    }

    public void setUrl(String url) {
        this.urlSuffix = url;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}