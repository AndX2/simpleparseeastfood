package com.github.andx2.eastfoods;

import com.github.andx2.eastfoods.pojo.Category;
import com.github.andx2.eastfoods.pojo.Food;
import com.google.gson.Gson;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        long startTime = Calendar.getInstance().getTimeInMillis();
        System.out.println("main run");
        String baseUrl = "http://xn----1-5cdbak7ffja2ahou3du8j.xn--p1ai";
        String listCategoriesCss = ".hikashop_container";
        List<Category> categoryList = new ArrayList();
        try {
            Document document = null;
            document = Jsoup.connect(baseUrl + "/menyu/category/").get();
            System.out.println("getting categories page: " +
                    (Calendar.getInstance().getTimeInMillis() - startTime)/1000f + "c");
//            System.out.println(document.title());
            Elements categories = document.select(listCategoriesCss);
            for (Element element:categories){
                Category category = new Category();
//                System.out.println(element.toString());
//                System.out.println("#############");
                category.setAvatar((element.select(".hikashop_product_listing_image")).attr("src"));
//                System.out.println((element.select(".hikashop_product_listing_image")).attr("src"));
//                System.out.println("#############");
                category.setTitle((element.select(".hikashop_category_name")).text());
//                System.out.println((element.select(".hikashop_category_name")).text());
//                System.out.println("#############");
                category.setUrlSuffix((element.select(".hikashop_category_image").select("a")).attr("href"));
//                System.out.println((element.select(".hikashop_category_image").select("a")).attr("href"));
//                System.out.println("#############");
                categoryList.add(category);
            }
        } catch (IOException e) {
            System.out.println("catch error Jsoup.connect()");
            e.printStackTrace();
        }

        for(Category category: categoryList){
            try {
                Document document = null;
                document = Jsoup.connect(baseUrl + category.getUrlSuffix()).get();
                Elements elements = document.select(".hikashop_container");
//                System.out.println(elements.toString());
                System.out.println(category.getTitle() + "\n");
                for (Element element:elements){
                    Food food =  new Food();
                    food.setTitle(element.select("a").attr("title"));
                    food.setUrl(element.select("a").attr("href"));
                    food.setAvatar((element.select(".hikashop_product_listing_image")).attr("src"));
                    Connection connection = Jsoup.connect(baseUrl + food.getUrl());
                    connection.timeout(30_000);
                    Document currentFoodDoc = connection.get();
                    food.setCost(currentFoodDoc.select(".hikashop_product_price").text());
                    food.setPic(currentFoodDoc.select("#hikashop_main_image").attr("src"));
                    System.out.println(currentFoodDoc.select("#hikashop_main_image").attr("src"));
//                    System.out.println(element.select("a").attr("title"));
//                    System.out.println(element.select("a").attr("href"));
//                    System.out.println((element.select(".hikashop_product_listing_image")).attr("src"));
                    category.getFoods().add(food);
                }
                System.out.println("#############");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        System.out.println(new Gson().toJson(categoryList));
        System.out.println("getting categories time: " +
                (Calendar.getInstance().getTimeInMillis() - startTime)/1000f + "c");



    }
}
